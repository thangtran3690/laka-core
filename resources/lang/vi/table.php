<?php

return [
    'no_item_found' => '<strong>Sorry!</strong> Không có dữ liệu',
    'total_items' => 'Tổng cộng',
    'index' => 'STT.',
    'btn_create' => 'Tạo mới',
    'btn_edit' => 'Chỉnh sửa',
    'btn_detail' => 'Chi tiết',
    'btn_delete' => 'Xóa',
    'btn_refresh' => 'Làm mới',
    'show_result' => 'Hiển thị %d đến %d trong số %d kết quả',
    'action'    => 'Hành động',
    'select'    => 'Chọn ...',
    'action_question_delete' => 'Bạn có chắc chắn muốn xóa?',
    'loading_text' => 'Đang tải',
    'save' => 'Lưu',
    'close' => 'Đóng'
];
