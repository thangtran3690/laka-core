@php
    $attrClose = array_merge([
        'text' => translate('table.close'),
        'icon' => 'fa fa-times',
        'size' => 'sm',
        'variant' => 'danger',
        'data-dismiss' => 'modal'
    ], data_get($data, 'close', []));
    $closeText = array_pull($attrClose, 'text');
    $closeVariant = array_pull($attrClose, 'variant');

    $attrOk = array_merge([
        'text' => translate('table.save'),
        'icon' => 'fa fa-save',
        'size' => 'sm',
        'variant' => 'success',
        'type' => 'submit',
        'data-loading' => translate('table.loading_text')
    ], data_get($data, 'ok', []));
    $okText = array_pull($attrOk, 'text');
    $typeOk = array_pull($attrOk, 'type');
    $okVariant = array_pull($attrOk, 'variant');
@endphp
{!! Form::btButton($closeText, $closeVariant, $attrClose) !!}

@if (!ends_with(get_route_name(), 'show'))
{!! Form::btButton($okText, $okVariant, $attrOk, '', '', $typeOk) !!}
@endif
