@php
$prefix = config('laka-core.prefix');
@endphp
@if ($paginator->hasPages())
<div class="paginatior d-flex justify-content-between">
    @includeWhen($showPageSize, "{$prefix}::components.tables.pagination.pager-dropdown")
    @include("{$prefix}::components.tables.pagination.paginarion-pager")
    @includeWhen($showInfo, "{$prefix}::components.tables.pagination.pager-info")
</div>
@endif

