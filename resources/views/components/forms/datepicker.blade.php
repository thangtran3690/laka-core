<div class="datepicker" {!! $attributes->merge(['name' => $name]) !!}></div>
@once
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/dx.web-light.css') }}">
@endpush
@push('scripts')
    <script src="https://cdn3.devexpress.com/jslib/22.1.4/js/dx.all.js" type="text/javascript"></script>
@endpush
@endonce
@push('scripts')
<script>
    $(".datepicker").dxDateBox({
        type: 'date',
        openOnFieldClick: true,
        name: '{{$name}}',
        displayFormat: '{{$dateFormat}}',
        elementAttr: @json($attributes->getAttributes()),
        calendarOptions: {
            maxZoomLevel: '{{$viewMode}}'
        },
        value: '{{ Carbon\Carbon::parse($value)->toString() }}'
    });
</script>
@endpush
