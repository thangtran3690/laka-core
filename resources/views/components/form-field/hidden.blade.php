@if ($showField)
    {!! Form::input($type, $name, $options['value'], array_except($options['attr'], ['wrapper'])) !!}
@endif
