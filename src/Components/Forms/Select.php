<?php

namespace Laka\Core\Components\Forms;

use Laka\Core\Components\Component;
use Laka\Core\Helpers\Classes;

class Select extends Component
{
    /**
     * The component alias name.
     *
     * @var string
     */
    public $componentName = 'form-select';

    public $name;
    public $class;
    public $help;
    public $groupClass;
    public $selected;
    public $items;
    public $optAttributes = [];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $items,
        string $name = '',
        string $class = '',
        $help = '',
        $groupClass = '',
        $selected = null,
        $size = null,
        $custom = false)
    {
        $this->items = $this->getOptions($items);
        $this->name = $name;
        $controlClass = sprintf('%s-control', $custom ? 'custom' : 'form');
        $classSize = $size ? sprintf('form-control-%s', $size) : '';
        $this->class = Classes::get([
            $controlClass,
            $class,
            $classSize
        ]);
        $this->help = $help;
        $this->groupClass = $groupClass;
        $this->selected = $selected;
    }

    protected function getOptions($items)
    {
        if (is_array($items) && !is_array(head($items))) return $items;
        
        return collect($items)->map(function ($item) {
            $option = array_only($item, ['id', 'name']);
            $this->optAttributes[data_get($option, 'id')] = array_except($item, ['id', 'name']);

            return $option;
        })->pluck('name', 'id')->toArray();
    }
}
