<?php

namespace Laka\Core\Components\Forms;

use Laka\Core\Components\Component;
use Laka\Core\Helpers\Classes;

class Datepicker extends Component
{
    public $name;
    public $dateFormat = 'yyyy-MM-dd';
    public $value;
    public $viewMode;

    /**
     * The component alias name.
     *
     * @var string
     */
    public $componentName = 'datepicker';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $dateFormat = 'yyyy-MM-dd', $size = null, $value = null, $viewMode = 'month')
    {
        $this->name = $name;
        $this->dateFormat = $dateFormat;
        $this->value = $value;
        $this->viewMode = $viewMode;
    }
}
