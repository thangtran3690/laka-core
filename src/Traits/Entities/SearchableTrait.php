<?php

namespace Laka\Core\Traits\Entities;

use Laka\Core\Entities\BaseBuilder;

/*
 * A trait to handle use fillable columns
 */

trait SearchableTrait
{
    public function getFillableColumns()
    {
        return $this->fillableColumns ?? ['*'];
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new BaseBuilder($query);
    }
}
